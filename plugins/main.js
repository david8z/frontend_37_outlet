import Vue from 'vue';
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';
import VueTelInput from 'vue-tel-input'
 
Vue.use(VueTelInput)
Vue.use(VuetifyGoogleAutocomplete, {
  apiKey: 'AIzaSyAUN8D0Tmk-sn7gpSG2ZMcM5jjaki_W_II', // Can also be an object. E.g, for Google Maps Premium API, pass `{ client: <YOUR-CLIENT-ID> }`
//   version: '...', // Optional
//   language: '...', // Optional
});