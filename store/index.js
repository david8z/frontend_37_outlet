import Vuex from 'vuex'
import axios from 'axios'

export const state= () => ({
    drawer:false,
    categories:[],
    wishlist:[],
    cart: [],
    alertColor:process.env.alertColor,
    pvpTotal: 0,
    prTotal: 0,
    deliveryCost: 0,
    valorEnviosGratis: process.env.valorEnviosGratis,
    user: null,
    anonymous_user:null,
    step: 0,
    scroll: {'active':false, 'offset': {}}
})
export const mutations= {
  scroll(state, newValue){
    if(typeof newValue === "boolean"){
      state.scroll.active = newValue 
    }
    else{
      state.scroll.offset[Object.keys(newValue)[0]]=Object.values(newValue)[0]
    }
    
  },

  anonymous_user(state, newValue){
    if(newValue != null){
      state.anonymous_user = {
        "first_name": newValue.first_name, 
        "last_name": newValue.last_name, 
        "mobile": newValue.mobile,
        "email": newValue.email
      }
    }else{
      state.anonymous_user = null
    }  
  },

  step(state, newValue){
    state.step=newValue 
  },

  user(state, newValue){
    if(newValue != null){
      state.user = {"first_name": newValue.first_name, 
        "last_name": newValue.last_name, 
        "mobile": newValue.mobile,
        "email": newValue.email}
    }else{
      state.user = null
    }  
  },

  wishlist(state, newValue){
    state.wishlist = newValue;
  },

  cart(state, newValue){
    state.cart = newValue;
  },

  pvpTotal(state, newValue){
    state.pvpTotal = newValue;
  },

  prTotal(state, newValue){
    state.prTotal = newValue;
  },

  deliveryCost(state, newValue){
    state.deliveryCost = newValue;
  },

  saveCategories(state, newValue){
    state.categories = newValue;
  },

  drawer(state, newValue){
    state.drawer = newValue;
  },

  cleanCategories(state){
    for(var key in state.categories){
      state.categories[key]['selected'] = false;
      for(var keyHijo in state.categories[key]['categorias_hijo']){
        state.categories[key]['categorias_hijo'][keyHijo]['selected']=false;
      }
    }
  },

  selectCategoryPadre(state, slugCategory) {
    for(var key in state.categories){
      if (state.categories[key]['slug'] == slugCategory){
        state.categories[key]['selected'] = true;
      }
      else{
        state.categories[key]['selected'] = false;
      }
      for(var keyHijo in state.categories[key]['categorias_hijo']){
        state.categories[key]['categorias_hijo'][keyHijo]['selected']=false;
      }
    }
  },
  
  selectCategoryHijo(state,slugHijo){
    for(var key in state.categories){
      state.categories[key]['selected'] = false;
      for(var keyHijo in state.categories[key]['categorias_hijo']){
        if(state.categories[key]['categorias_hijo'][keyHijo]['slug']==slugHijo){
          state.categories[key]['categorias_hijo'][keyHijo]['selected']=true;
          state.categories[key]['selected'] = true;
        }
        else{
          state.categories[key]['categorias_hijo'][keyHijo]['selected']=false;
        }
      }
    }
  },  
}

export const actions ={
  userAuthenticated(context, userData){

    this.$cookies.set('user-token', userData.token, {
      path: '/',
      maxAge: process.env.cookieAge
    })

    // WE SET USER CART AND WISHLIST
    var headers = {
      'Authorization': 'Token ' + userData.token
    }
    axios.put("http://localhost:8000/api/cart_wishlist/" , {},  {withCredentials: true, headers:headers})
    .then(res =>{
      context.commit('user', userData)
      context.commit('anonymous_user', res.data.anonymous_user)
      context.commit('wishlist', res.data.wishlist)
      context.commit('cart', res.data.cart)
      context.commit('pvpTotal', res.data.pvp_total)
      context.commit('prTotal', res.data.pr_total)
      context.commit('deliveryCost', res.data.delivery_cost)
      
    })
    .catch(e => context.error(e))

    // Eliminamos el cart token
    this.$cookies.remove('cart-wishlist-token')
  },

  addToWishlist(context, slugProducto){
    var headers = {}
    if(this.$cookies.get('user-token')){
      headers = {
        'Authorization': 'Token ' + this.$cookies.get('user-token')
      }
    }

    return axios.put("http://localhost:8000/api/cart_wishlist/" , 
    {"wishlist":slugProducto},  
    {withCredentials: true, headers:headers})
    .then(res =>{
      context.commit('wishlist', res.data.wishlist)
    })
    .catch(e => context.error(e))
  },

  addToCart(context, slugProducto){
    var headers = {}
    if(this.$cookies.get('user-token')){
      headers = {
        'Authorization': 'Token ' + this.$cookies.get('user-token')
      }
    }

    axios.put("http://localhost:8000/api/cart_wishlist/" , 
    {"cart":slugProducto},  
    {withCredentials: true, headers:headers})
    .then(res =>{
      context.commit('cart', res.data.cart)
      context.commit('pvpTotal', res.data.pvp_total)
      context.commit('prTotal', res.data.pr_total)
      context.commit('deliveryCost', res.data.delivery_cost)
    })
    .catch(e => context.error(e))
  },

  logoutUser(context){
    axios.put("http://localhost:8000/api/cart_wishlist/" , {})
    .then(res =>{
      this.$cookies.set('cart-wishlist-token', res.data.id, {
        path: '/',
        maxAge: process.env.cookieAge
      })
      context.commit('anonymous_user', null)
      context.commit('user', null)
      context.commit('wishlist', [])
      context.commit('cart', [])
      context.commit('pvpTotal', 0)
      context.commit('prTotal', 0)
      context.commit('deliveryCost', 3.99)
      context.commit('step',1)
    })
    .catch(e => console.log(e))
  },
  
  nuxtClientInit(vuexContext, context){
  },
    
  nuxtServerInit(vuexContext, context){
    
    // DECLARE HEADER
    var headers = {}

    if(this.$cookies.get('user-token') != undefined){
      headers = {
        'Authorization': 'Token ' + this.$cookies.get('user-token')
      }
      axios.get("http://localhost:8000/api-token-auth/", {headers:headers})
      .then(res =>{
        vuexContext.commit('user', res.data)
      })
      .catch(e => console.log("error"))
    }
    else if (this.$cookies.get('cart-wishlist-token') != undefined){
      headers = {
        Cookie: "cart-wishlist-token = " + this.$cookies.get('cart-wishlist-token') + ";"
      }
      vuexContext.commit('user', null)
    }
    

    // WE SET USER OR ANONYMOUS USER AND CART AND WISHLIST
    var cartAndWishlistPromise = axios.put("http://localhost:8000/api/cart_wishlist/" , {},  {headers:headers})
    .then(res =>{

      if(this.$cookies.get('user-token') != undefined){
        // Se usa para actualizar el max Age
        this.$cookies.set('user-token', this.$cookies.get('user-token'), {
          path: '/',
          maxAge: process.env.cookieAge
        })
      }
      else{
        this.$cookies.set('cart-wishlist-token', res.data.id, {
          path: '/',
          maxAge: process.env.cookieAge
        })
      }
      vuexContext.commit('anonymous_user', res.data.anonymous_user)
      vuexContext.commit('wishlist', res.data.wishlist)
      vuexContext.commit('cart', res.data.cart)
      vuexContext.commit('pvpTotal', res.data.pvp_total)
      vuexContext.commit('prTotal', res.data.pr_total)
      vuexContext.commit('deliveryCost', res.data.delivery_cost)
    })
    .catch(e => context.error(e))
    
    // WE GET CATEGORIES
    var categoriesPromise = axios.get("http://localhost:8000/api/categoria_padre/" ,  {withCredentials: true})
    .then(res =>{
      vuexContext.commit('saveCategories', res.data)
    })
    .catch(e => context.error(e))

    return Promise.all([cartAndWishlistPromise, categoriesPromise])
  },
}

export const getters= {
  drawer: state => state.drawer,
  categories: state => state.categories,
  wishlist: state => state.wishlist,
  cart: state => state.cart,
  pvpTotal: state => state.pvpTotal,
  prTotal: state => state.prTotal,
  deliveryCost: state => state.deliveryCost,
  alertColor: state => state.alertColor,
  valorEnviosGratis: state => state.valorEnviosGratis,
  user: state => state.user,
  anonymous_user: state => state.anonymous_user,
  step: state => state.step,
  scroll: state => state.scroll,
  authenticated: state => state.user != null,
}
