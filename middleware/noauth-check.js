export default function (context){
    if(!context.app.$cookies.get('user-token')){
        return context.redirect('/cuenta/login')
    }   
}